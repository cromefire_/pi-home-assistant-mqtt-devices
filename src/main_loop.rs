use crate::devices::light::Light;
use crate::lights::update_light_state;
use crate::{Config, TopicHelper};
use async_std::sync::{Arc, Mutex};
use kv::Store;
use rumqttc::Event::{Incoming, Outgoing};
use rumqttc::Packet::{PingResp, Publish as PublishNotification};
use rumqttc::{AsyncClient, ConnectionError, EventLoop, Publish};
use std::collections::HashMap;
use std::fs::{File};
use std::sync::Arc as SyncArc;
use std::time::Duration;
use rumqttc::Outgoing::PingReq;
use tokio::time;
use crate::activity_led::{led_off, led_on};

pub async fn run(
    mut eventloop: EventLoop,
    config: SyncArc<Config>,
    client: Arc<Mutex<AsyncClient>>,
    store: &Store,
    lights: &mut HashMap<String, Box<dyn Light>>,
    led_file: &mut Option<File>,
) {
    let th = TopicHelper::new(&config);
    let light_prefix = format!("{}/light/", th.prefix());

    log::info!("Starting Loop");
    loop {
        let poll_result = eventloop.poll().await;
        if let Err(err) = poll_result {
            match err {
                ConnectionError::MqttState(s) => {
                    log::warn!("MQTT server closed the connection: {:?}", s)
                }
                ConnectionError::NetworkTimeout => log::warn!("Network timed out"),
                ConnectionError::FlushTimeout => log::warn!("Flush timed out"),
                ConnectionError::ConnectionRefused(n) => {
                    log::warn!("Failed to connect to MQTT server: {:?}", n)
                }
                ConnectionError::Tls(err) => {
                    log::warn!("Failed to negotiate TLS with MQTT server: {:?}", err)
                }
                ConnectionError::Io(err) => {
                    log::warn!("Failed to connect to MQTT server with I/O error: {:?}", err)
                }
                ConnectionError::NotConnAck(_) => {
                    log::warn!("Failed to connect to MQTT server: Connection not acknowledged")
                }
                ConnectionError::RequestsDone => {}
            }
            return;
        }
        let notification = poll_result.unwrap();
        if let Incoming(incoming) = notification {
            if let PublishNotification(publ) = incoming {
                if publ.topic == config.topics.status {
                    process_status(&publ).await;
                } else if publ.topic.starts_with(&light_prefix) {
                    let topic_postfix = publ.topic.strip_prefix(&light_prefix).unwrap();
                    if let Some(id) = topic_postfix.strip_suffix("/set") {
                        led_on(led_file);

                        let client = client.lock().await;
                        process_light_cmd(config.clone(), &client, id, &publ, store, lights).await;

                        led_off(led_file);
                    }
                } else {
                    log::warn!("Received messages on invalid topic: {}", publ.topic);
                }
            } else if incoming == PingResp {
                led_on(led_file);

                time::sleep(Duration::from_millis(1)).await;
                log::trace!("Pong received");

                led_off(led_file);
            } else {
                log::trace!("Received = {:?}", incoming);
            }
        } else if let Outgoing(outgoing) = notification {
            if outgoing == PingReq {
                led_on(led_file);

                time::sleep(Duration::from_millis(1)).await;
                log::trace!("Ping sent");

                led_off(led_file);
            } else {
                log::trace!("Sent = {:?}", outgoing);
            }
        }
    }
}

async fn process_status(publ: &Publish) {
    let payload = String::from_utf8(publ.payload.to_vec()).unwrap();
    if payload == "online" {
        log::info!("Home Assistant online");
    } else if payload == "offline" {
        log::info!("Home Assistant offline");
    } else {
        log::warn!("Invalid Home Assistant status: {}", payload);
    }
}

async fn process_light_cmd(
    config: SyncArc<Config>,
    client: &AsyncClient,
    id: &str,
    publ: &Publish,
    store: &Store,
    lights: &mut HashMap<String, Box<dyn Light>>,
) {
    let light_opt = lights.get_mut(id);
    if light_opt.is_none() {
        log::warn!("Received command for non-existent device: {}", id);
        return;
    }
    let light = light_opt.unwrap();
    let payload = String::from_utf8(publ.payload.to_vec()).unwrap();
    let result = light.apply_new_state(payload).await;
    if let Err(err) = result {
        log::warn!("Failed to set light state: {:?}", err)
    }
    let new_state = light.current_state();
    let bucket = store.bucket(Some("light")).unwrap();
    let result = update_light_state(&config, client, &bucket, id, new_state).await;
    if let Err(err) = result {
        log::warn!("Failed to send light state: {:?}", err)
    }
}
