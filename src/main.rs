use crate::activity_led::{led_off, led_on, open_activity_led_file};
use crate::adc::load_analog_inputs;
use crate::app_info::VERSION;
use crate::config::{load_config, AnalogInputs, Config, PwmModule};
use crate::devices::light::load_lights;
use crate::devices::switch::load_switches;
use crate::hass::lights;
use crate::lights::register_light;
use crate::pwm::{load_pwm_modules, Pwm};
use crate::topics::TopicHelper;
use async_std::sync::{Arc, Mutex};
use kv::Store;
use std::env;
use std::process::exit;
use std::sync::Arc as SyncArc;
use std::time::Duration;
use tokio::time;

mod activity_led;
mod adc;
mod app_info;
mod config;
//mod debouncing;
mod devices;
mod hass;
mod main_loop;
mod pwm;
mod startup;
mod topics;

#[tokio::main]
async fn main() {
    pretty_env_logger::init();
    log::info!("Version {}", VERSION);

    #[allow(clippy::needless_collect)]
    let args: Vec<String> = env::args().collect();
    if args.contains(&"--help".to_string()) {
        eprintln!("hass-mqtt-deviced  # No options");
        exit(0);
    }

    let config = load_config();

    let pwm_modules = load_pwm_modules(&config).await;
    let analog_inputs = load_analog_inputs(&config);

    let mut store_config = kv::Config::new("./states");
    store_config.cache_capacity = Some(1048576);
    let store = Store::new(store_config).unwrap();

    let mut lights = load_lights(&config, &store, &pwm_modules).await;
    let switches = load_switches(&config, &analog_inputs).await;

    let mut led_file = open_activity_led_file(config.activity_led.clone());

    let cfg = SyncArc::new(config);
    loop {
        let (eventloop, client) = startup::start(cfg.clone()).await;
        let client = Arc::new(Mutex::new(client));
        startup::register_devices(cfg.clone(), client.clone(), &store, &lights, &switches).await;
        main_loop::run(
            eventloop,
            cfg.clone(),
            client.clone(),
            &store,
            &mut lights,
            &mut led_file,
        )
        .await;
        log::info!("Waiting 5s before reconnecting...");
        led_off(&mut led_file);
        time::sleep(Duration::from_millis(500)).await;
        led_on(&mut led_file);
        time::sleep(Duration::from_secs(1)).await;
        led_off(&mut led_file);
        time::sleep(Duration::from_secs(1)).await;
        led_on(&mut led_file);
        time::sleep(Duration::from_secs(1)).await;
        led_off(&mut led_file);
        time::sleep(Duration::from_secs(1)).await;
        led_on(&mut led_file);
        time::sleep(Duration::from_millis(500)).await;
        led_off(&mut led_file);
        log::info!("Reconnecting...");
    }
}
