use crate::hass::{get_device_info, HassQos, HassRegistration};
use crate::{Config, TopicHelper};
use rumqttc::{AsyncClient, QoS};
use crate::config::DeviceMetadata;

pub async fn register_switch(config: &Config, client: &AsyncClient, id: &str, meta: &DeviceMetadata) {
    let th = TopicHelper::new(config);
    let topic = th.discovery("binary_sensor", id);
    let prefix = th.device_prefix("binary_sensor", id);

    let msg = HassRegistration {
        prefix: &prefix,
        name: &meta.name,
        unique_id: id,
        schema: None,
        state_topic: "~/status",
        payload_on: Some("1"),
        payload_off: Some("0"),
        command_topic: None,
        availability_topic: &th.activity(),
        brightness: None,
        color_mode: None,
        supported_color_modes: None,
        min_mireds: None,
        max_mireds: None,
        qos: HassQos::AtLeastOnce,
        optimistic: false,
        device: &get_device_info(config),
    };

    let payload = serde_json::to_string(&msg).unwrap();

    client
        .publish(topic, QoS::AtLeastOnce, true, payload)
        .await
        .unwrap();
}
