use std::error::Error;
use kv::{Bucket, Msgpack};
use super::{get_device_info, HassQos, HassRegistration};
use crate::devices::light::{ColorMode, LightState};
use crate::{Config, TopicHelper};
use rumqttc::{AsyncClient, ClientError, QoS};
use serde::{Deserialize, Serialize};
use thiserror::Error;
use crate::config::DeviceMetadata;

pub async fn register_light(
    config: &Config,
    client: &AsyncClient,
    id: &str,
    light_config: HassLightConfig,
    meta: &DeviceMetadata,
) {
    let th = TopicHelper::new(config);
    let topic = th.discovery("light", id);
    let prefix = th.device_prefix("light", id);

    let msg = HassRegistration {
        prefix: &prefix,
        name: &meta.name,
        unique_id: id,
        schema: Some("json"),
        state_topic: "~/status",
        payload_on: None,
        payload_off: None,
        command_topic: Some("~/set"),
        availability_topic: &th.activity(),
        brightness: Some(light_config.brightness),
        color_mode: Some(light_config.color_mode),
        supported_color_modes: light_config.supported_color_modes,
        min_mireds: light_config.min_mireds,
        max_mireds: light_config.max_mireds,
        qos: HassQos::AtLeastOnce,
        optimistic: false,
        device: &get_device_info(config),
    };

    let payload = serde_json::to_string(&msg).unwrap();

    client
        .publish(topic, QoS::AtLeastOnce, true, payload)
        .await
        .unwrap();
}

pub async fn update_light_state(
    config: &Config,
    client: &AsyncClient,
    bucket: &Bucket<'_, String, Msgpack<LightState>>,
    id: &str,
    state: LightState,
) -> Result<(), Box<dyn Error>> {
    let th = TopicHelper::new(config);
    let topic = th.tx("light", id);

    let msg = state_to_hass_state(&state, true);

    let payload = serde_json::to_string(&msg)?;

    client
        .publish(topic, QoS::ExactlyOnce, true, payload)
        .await?;
    bucket.set(&id.to_string(), &Msgpack(state.clone())).unwrap();
    bucket.flush_async().await.unwrap();
    log::debug!("Updated state of light {}: {:?}", id, state);

    Ok(())
}

pub fn get_initial_light_state(
    id: &str,
    bucket: &Bucket<'_, String, Msgpack<LightState>>,
) -> Result<LightState, Box<dyn Error>> {
    let res = bucket.get(&id.to_string())?;
    if let Some(value) = res {
        Ok(value.0)
    } else {
        log::warn!("Cannot find initial value for light {}", id);
        Ok(LightState::Off(None))
    }
}

fn state_to_hass_state(state: &LightState, not_off_run: bool) -> HassLightState {
    match state {
        LightState::Off(ls) => {
            if !not_off_run {
                panic!("Off cannot be last state of Off")
            }
            if let Some(ls) = ls {
                state_to_hass_state(ls, false)
            } else {
                HassLightState {
                    state: OnOff::Off,
                    color_temp: None,
                    brightness: None,
                    color: None,
                    color_mode: ColorMode::OnOff,
                }
            }
        }
        LightState::On => HassLightState {
            state: not_off_run.into(),
            color_temp: None,
            brightness: None,
            color: None,
            color_mode: ColorMode::OnOff,
        },
        LightState::Brightness(b) => HassLightState {
            state: not_off_run.into(),
            color_temp: None,
            brightness: Some(*b),
            color: None,
            color_mode: ColorMode::Brightness,
        },
        LightState::ColorTemp(cct_state) => HassLightState {
            state: not_off_run.into(),
            color_temp: Some(cct_state.color_temp),
            brightness: Some(cct_state.brightness),
            color: None,
            color_mode: ColorMode::ColorTemp,
        },
        LightState::Rgb(rgb_state) => HassLightState {
            state: not_off_run.into(),
            color_temp: None,
            brightness: Some(rgb_state.brightness),
            color: Some(HassLightRgb {
                r: rgb_state.red,
                g: rgb_state.green,
                b: rgb_state.blue,
                c: None,
                w: None,
            }),
            color_mode: ColorMode::Rgb,
        },
        LightState::Rgbw(rgbw_state) => HassLightState {
            state: not_off_run.into(),
            color_temp: None,
            brightness: Some(rgbw_state.brightness),
            color: Some(HassLightRgb {
                r: rgbw_state.red,
                g: rgbw_state.green,
                b: rgbw_state.blue,
                c: None,
                w: Some(rgbw_state.white),
            }),
            color_mode: ColorMode::Rgbw,
        },
        LightState::Rgbww(rgbww_state) => HassLightState {
            state: not_off_run.into(),
            color_temp: None,
            brightness: Some(rgbww_state.brightness),
            color: Some(HassLightRgb {
                r: rgbww_state.red,
                g: rgbww_state.green,
                b: rgbww_state.blue,
                c: Some(rgbww_state.cold),
                w: Some(rgbww_state.warm),
            }),
            color_mode: ColorMode::Rgbww
        },
    }
}

#[derive(Debug, PartialEq, Serialize)]
pub struct HassLightConfig {
    pub brightness: bool,
    pub color_mode: bool,
    pub supported_color_modes: Option<Vec<ColorMode>>,
    pub min_mireds: Option<u16>,
    pub max_mireds: Option<u16>,
}

#[derive(Debug, PartialEq, Serialize)]
pub struct HassLightState {
    state: OnOff,
    color_temp: Option<u16>,
    brightness: Option<u8>,
    color: Option<HassLightRgb>,
    color_mode: ColorMode,
}

#[derive(Debug, PartialEq, Deserialize)]
pub struct HassLightDimmableCmd {
    pub state: OnOff,
    pub brightness: Option<u8>,
}

#[derive(Debug, PartialEq, Deserialize)]
pub struct HassLightRgbCmd {
    pub state: OnOff,
    pub brightness: Option<u8>,
    pub color: Option<HassLightRgb>,
    pub color_temp: Option<u16>,
}

#[derive(Debug, PartialEq, Serialize, Deserialize)]
pub enum OnOff {
    #[serde(rename = "ON")]
    On,
    #[serde(rename = "OFF")]
    Off,
}

impl From<bool> for OnOff {
    fn from(value: bool) -> Self {
        if value {
            OnOff::On
        } else {
            OnOff::Off
        }
    }
}

impl Into<bool> for OnOff {
    fn into(self) -> bool {
        match self {
            OnOff::On => true,
            OnOff::Off => false,
        }
    }
}

#[derive(Debug, PartialEq, Deserialize, Serialize, Copy, Clone)]
pub struct HassLightRgb {
    pub r: u8,
    pub g: u8,
    pub b: u8,
    pub c: Option<u8>,
    pub w: Option<u8>,
}

#[derive(Error, Debug)]
pub enum HassLightUpdateError {
    #[error("MQTT client error")]
    MqttClientError(#[from] ClientError),
    #[error("JSON format error")]
    SerdeError(#[from] serde_json::Error),
}
