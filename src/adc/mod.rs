use crate::{AnalogInputs, Config};
use async_std::sync::{Arc, Mutex};
use async_trait::async_trait;
use std::error::Error;

pub mod mcp3000;

pub fn load_analog_inputs(config: &Config) -> Vec<Arc<Mutex<dyn Adc + Send>>> {
    let mut analog_inputs: Vec<Arc<Mutex<dyn Adc + Send>>> = vec![];
    for ai in &config.analog_inputs {
        match ai {
            AnalogInputs::MCP3002 { bus, chip_select } => {
                mcp3000::validate_spi_parameters(*bus, *chip_select);
                analog_inputs.push(Arc::new(Mutex::new(mcp3000::Mcp3002::new(
                    *bus,
                    *chip_select,
                ))));
            }
            AnalogInputs::MCP3004 { bus, chip_select } => {
                mcp3000::validate_spi_parameters(*bus, *chip_select);
                analog_inputs.push(Arc::new(Mutex::new(mcp3000::Mcp3004::new(
                    *bus,
                    *chip_select,
                ))));
            }
            AnalogInputs::MCP3008 { bus, chip_select } => {
                mcp3000::validate_spi_parameters(*bus, *chip_select);
                analog_inputs.push(Arc::new(Mutex::new(mcp3000::Mcp3008::new(
                    *bus,
                    *chip_select,
                ))));
            }
            AnalogInputs::MCP3208 { bus, chip_select } => {
                mcp3000::validate_spi_parameters(*bus, *chip_select);
                analog_inputs.push(Arc::new(Mutex::new(mcp3000::Mcp3208::new(
                    *bus,
                    *chip_select,
                ))));
            }
        }
    }

    analog_inputs
}

#[async_trait]
pub trait Adc {
    async fn listen_analog(
        &mut self,
        pin: u8,
        callback: Box<dyn FnMut(u16) + Send>,
    ) -> Result<(), Box<dyn Error>>;

    fn max_pin(&self) -> u8;
    fn max_value(&self) -> u16;
}
