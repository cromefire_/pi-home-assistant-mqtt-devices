pub struct Debouncer {
    pattern: u8,
}

#[derive(PartialEq)]
pub enum DebounceResult {
    NoChange,
    Pressed,
    Released,
}

// https://github.com/TyberiusPrime/debouncing/blob/51e17f164480cef12f6c0eb5d8e9760d0e2c2fbb/src/lib.rs
impl Debouncer {
    pub fn new() -> Debouncer {
        Debouncer { pattern: 0 }
    }

    pub fn update(&mut self, pressed: bool) -> DebounceResult {
        let next: u8 = if pressed { 1 } else { 0 };
        self.pattern = self.pattern << 1 | next;
        let mut result = DebounceResult::NoChange;
        //debounce following hackadays ultimate debouncing schema
        let mask: u8 = 0b11000111;
        let seen = self.pattern & mask;
        if seen == 0b00000111 {
            result = DebounceResult::Pressed;
            self.pattern = 0b1111111;
        } else if seen == 0b11000000 {
            result = DebounceResult::Released;
            self.pattern = 0b0000000;
        }

        result
    }
}

#[cfg(test)]
mod tests {
    use super::{DebounceResult, Debouncer};
    #[test]
    fn it_works() {
        let mut db = Debouncer::new();
        //activate
        assert!(db.update(true) == DebounceResult::NoChange);
        assert!(db.update(true) == DebounceResult::NoChange);
        assert!(db.update(true) == DebounceResult::Pressed);
        //deactivate
        assert!(db.update(false) == DebounceResult::NoChange);
        assert!(db.update(false) == DebounceResult::NoChange);
        assert!(db.update(false) == DebounceResult::Released);

        //let's do noise.
        assert!(db.update(true) == DebounceResult::NoChange);
        assert!(db.update(false) == DebounceResult::NoChange);
        assert!(db.update(false) == DebounceResult::NoChange);
        assert!(db.update(false) == DebounceResult::NoChange);
        assert!(db.update(false) == DebounceResult::NoChange);
        assert!(db.update(false) == DebounceResult::NoChange);
        assert!(db.update(false) == DebounceResult::NoChange);

        assert!(db.update(true) == DebounceResult::NoChange);
        assert!(db.update(true) == DebounceResult::NoChange);
        assert!(db.update(true) == DebounceResult::Pressed);
        assert!(db.update(true) == DebounceResult::NoChange);
        assert!(db.update(false) == DebounceResult::NoChange);
        assert!(db.update(false) == DebounceResult::NoChange);
        assert!(db.update(true) == DebounceResult::NoChange);
        assert!(db.update(true) == DebounceResult::NoChange);
        assert!(db.update(true) == DebounceResult::NoChange);
    }
}
