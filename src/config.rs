use std::collections::HashMap;
use serde::Deserialize;
use std::fs::File;
use std::io::{BufReader, ErrorKind};
use std::process::exit;

pub fn load_config() -> Config {
    let f = File::open("config.yaml");
    if let Err(err) = f {
        if err.kind() == ErrorKind::NotFound {
            log::error!("Config file \"config.yaml\" not found.");
            exit(1);
        }
        log::error!("{}", err);
        exit(1)
    }
    let config_result: Result<Config, _> = serde_yaml::from_reader(BufReader::new(f.unwrap()));
    let config = if let Err(err) = config_result {
        log::error!("{}", err);
        exit(1)
    } else {
        config_result.unwrap()
    };

    config
}

#[derive(Debug, PartialEq, Deserialize)]
pub struct Config {
    pub id: String,
    pub name: String,
    pub activity_led: Option<String>,
    pub connection: ConnectionConfig,
    pub topics: TopicsConfig,
    #[serde(default)]
    pub pwm_modules: Vec<PwmModule>,
    #[serde(default)]
    pub analog_inputs: Vec<AnalogInputs>,
    pub devices: Devices,
}

#[derive(Debug, PartialEq, Deserialize)]
pub struct ConnectionConfig {
    pub host: String,
    #[serde(default = "defaults::port")]
    pub port: u16,
    pub username: String,
    pub password: String,
}

#[derive(Debug, PartialEq, Deserialize)]
pub struct TopicsConfig {
    #[serde(default = "defaults::discovery")]
    pub discovery: String,
    #[serde(default = "defaults::prefix")]
    pub prefix: String,
    #[serde(default = "defaults::status")]
    pub status: String,
}

#[derive(Debug, PartialEq, Deserialize)]
#[serde(tag = "type")]
pub enum PwmModule {
    PiGpio,
    PCA9685 { bus: u8, address: u8 },
}

#[derive(Debug, PartialEq, Deserialize)]
#[serde(tag = "type")]
pub enum AnalogInputs {
    MCP3002 { bus: u8, chip_select: u8 },
    MCP3004 { bus: u8, chip_select: u8 },
    MCP3008 { bus: u8, chip_select: u8 },
    MCP3208 { bus: u8, chip_select: u8 },
}

#[derive(Debug, PartialEq, Deserialize)]
pub struct Devices {
    #[serde(default)]
    pub lights: HashMap<String, Lights>,
    #[serde(default)]
    pub switches: HashMap<String, Switches>,
}

#[derive(Debug, PartialEq, Deserialize)]
#[serde(tag = "type")]
pub enum Lights {
    // Basic {},  // Currently unsupported
    Dimmable {
        channel: PwmLightChannelConfig,
        meta: DeviceMetadata,
    },
    Cct {
        cold: PwmLightChannelConfig,
        warm: PwmLightChannelConfig,
        color_temp_config: ColorTempConfig,
        meta: DeviceMetadata,
    },
    Rgb {
        red: PwmLightChannelConfig,
        green: PwmLightChannelConfig,
        blue: PwmLightChannelConfig,
        meta: DeviceMetadata,
    },
    Rgbw {
        red: PwmLightChannelConfig,
        green: PwmLightChannelConfig,
        blue: PwmLightChannelConfig,
        white: PwmLightChannelConfig,
        meta: DeviceMetadata,
    },
    RgbCct {
        red: PwmLightChannelConfig,
        green: PwmLightChannelConfig,
        blue: PwmLightChannelConfig,
        cold: PwmLightChannelConfig,
        warm: PwmLightChannelConfig,
        color_temp_config: ColorTempConfig,
        meta: DeviceMetadata,
    },
}

#[derive(Debug, PartialEq, Deserialize)]
#[serde(tag = "type")]
pub enum Switches {
    /*BasicButton {
        input: DigitalInputConfig,
        #[serde(default = "defaults::debouncing")]
        debouncing: bool,
        meta: DeviceMetadata,
    },*/
    AnalogButton {
        analog: AnalogInputConfig,
        threshold: u16,
        #[serde(default = "defaults::debouncing")]
        debouncing: bool,
        meta: DeviceMetadata,
    },
}

#[derive(Debug, PartialEq, Deserialize)]
pub struct PwmLightChannelConfig {
    pub output: u8,
    pub pin: u8,
    pub min: u16,
    pub max: u16,
    #[serde(default)]
    pub curve: Option<String>,
}

#[derive(Debug, PartialEq, Deserialize)]
pub struct AnalogInputConfig {
    pub input: u8,
    pub pin: u8,
}

#[derive(Debug, PartialEq, Deserialize, Clone)]
pub struct DeviceMetadata {
    pub name: String,
    #[serde(default)]
    pub icon: Option<String>,
}

#[derive(Debug, PartialEq, Deserialize, Clone)]
pub struct ColorTempConfig {
    pub min_mireds: u16,
    pub max_mireds: u16,
}

// Defaults
mod defaults {
    pub const fn port() -> u16 {
        1883
    }
    pub fn discovery() -> String {
        "homeassistant".to_string()
    }
    pub fn prefix() -> String {
        "devices".to_string()
    }
    pub fn status() -> String {
        "homeassistant/status".to_string()
    }
    pub fn debouncing() -> bool {
        true
    }
}
