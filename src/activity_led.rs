use std::fs::File;
use std::io::Write;

pub fn open_activity_led_file(name: Option<String>) -> Option<File> {
    name.map(|p| {
        File::create(format!("/sys/class/leds/{}/invert", p)).expect("LED file not found")
    })
}

pub fn led_on(led_file: &mut Option<File>) {
    if let Some(lf) = led_file {
        lf.write_all("1".as_bytes()).expect("Enable LED");
    }
}

pub fn led_off(led_file: &mut Option<File>) {
    if let Some(lf) = led_file {
        lf.write_all("0".as_bytes()).expect("Disable LED");
    }
}
