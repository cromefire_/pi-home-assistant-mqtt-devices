use super::{get_channel_brightness, ColorMode, Light, LightState, PwmChannel, RgbwwState};
use crate::config::{ColorTempConfig, DeviceMetadata};
use crate::devices::light::{get_brightness_from_state, CctState, RgbState, RgbwState};
use crate::hass::lights::{HassLightConfig, HassLightRgbCmd};
use async_trait::async_trait;
use std::error::Error;
use std::sync::{Arc, Mutex};
use thiserror::Error;

pub struct RgbCctLight {
    id: String,
    red: Arc<Mutex<PwmChannel>>,
    green: Arc<Mutex<PwmChannel>>,
    blue: Arc<Mutex<PwmChannel>>,
    cold: Arc<Mutex<PwmChannel>>,
    warm: Arc<Mutex<PwmChannel>>,
    state: LightState,
    color_temp_config: ColorTempConfig,
    meta: DeviceMetadata,
}

impl RgbCctLight {
    pub fn new(
        id: String,
        red: PwmChannel,
        green: PwmChannel,
        blue: PwmChannel,
        cold: PwmChannel,
        warm: PwmChannel,
        state: LightState,
        color_temp_config: ColorTempConfig,
        meta: DeviceMetadata,
    ) -> RgbCctLight {
        RgbCctLight {
            id,
            red: Arc::new(Mutex::new(red)),
            green: Arc::new(Mutex::new(green)),
            blue: Arc::new(Mutex::new(blue)),
            cold: Arc::new(Mutex::new(cold)),
            warm: Arc::new(Mutex::new(warm)),
            state,
            color_temp_config,
            meta,
        }
    }

    fn set_red(&self, value: f32) -> Result<(), Box<dyn Error>> {
        log::debug!("Setting red channel to {}", value);
        let red = self.red.lock().unwrap();
        red.set_value(value)
    }

    fn set_green(&self, value: f32) -> Result<(), Box<dyn Error>> {
        log::debug!("Setting green channel to {}", value);
        let green = self.green.lock().unwrap();
        green.set_value(value)
    }

    fn set_blue(&self, value: f32) -> Result<(), Box<dyn Error>> {
        log::debug!("Setting blue channel to {}", value);
        let blue = self.blue.lock().unwrap();
        blue.set_value(value)
    }

    fn set_cold(&self, value: f32) -> Result<(), Box<dyn Error>> {
        log::debug!("Setting cold channel to {}", value);
        let cold = self.cold.lock().unwrap();
        cold.set_value(value)
    }

    fn set_warm(&self, value: f32) -> Result<(), Box<dyn Error>> {
        log::debug!("Setting warm channel to {}", value);
        let warm = self.warm.lock().unwrap();
        warm.set_value(value)
    }

    fn apply_rgbww_state(&mut self, state: RgbwwState) -> Result<(), Box<dyn Error>> {
        let red_value = get_channel_brightness(state.red, state.brightness);
        let green_value = get_channel_brightness(state.green, state.brightness);
        let blue_value = get_channel_brightness(state.blue, state.brightness);
        let cold_value = get_channel_brightness(state.cold, state.brightness);
        let warm_value = get_channel_brightness(state.warm, state.brightness);

        self.set_red(red_value)?;
        self.set_green(green_value)?;
        self.set_blue(blue_value)?;
        self.set_cold(cold_value)?;
        self.set_warm(warm_value)?;
        self.state = LightState::Rgbww(state);

        Ok(())
    }

    fn apply_rgbw_state(&mut self, state: RgbwState) -> Result<(), Box<dyn Error>> {
        let red_value = get_channel_brightness(state.red, state.brightness);
        let green_value = get_channel_brightness(state.green, state.brightness);
        let blue_value = get_channel_brightness(state.blue, state.brightness);
        let white_value = get_channel_brightness(state.white, state.brightness);

        self.set_red(red_value)?;
        self.set_green(green_value)?;
        self.set_blue(blue_value)?;
        self.set_cold(white_value)?;
        self.set_warm(white_value)?;
        self.state = LightState::Rgbw(state);

        Ok(())
    }

    fn apply_rgb_state(&mut self, state: RgbState) -> Result<(), Box<dyn Error>> {
        let red_value = get_channel_brightness(state.red, state.brightness);
        let green_value = get_channel_brightness(state.green, state.brightness);
        let blue_value = get_channel_brightness(state.blue, state.brightness);

        self.set_red(red_value)?;
        self.set_green(green_value)?;
        self.set_blue(blue_value)?;
        self.set_cold(0_f32)?;
        self.set_warm(0_f32)?;
        self.state = LightState::Rgb(state);

        Ok(())
    }

    fn apply_cct_state(&mut self, state: CctState) -> Result<(), Box<dyn Error>> {
        if self.color_temp_config.min_mireds > state.color_temp
            || self.color_temp_config.max_mireds < state.color_temp
        {
            return Err(Box::new(RgbCctLightError::InvalidMiredCommand));
        }
        let brightness_fraction = state.brightness as f32 / 255_f32;
        let delta_mired =
            (self.color_temp_config.max_mireds - self.color_temp_config.min_mireds) as f32;
        let cold_value = (self.color_temp_config.max_mireds - state.color_temp) as f32
            / delta_mired
            * brightness_fraction;
        let warm_value = (state.color_temp - self.color_temp_config.min_mireds) as f32
            / delta_mired
            * brightness_fraction;

        self.set_red(0f32)?;
        self.set_green(0f32)?;
        self.set_blue(0f32)?;
        self.set_cold(cold_value)?;
        self.set_warm(warm_value)?;
        self.state = LightState::ColorTemp(state);

        Ok(())
    }
}

#[async_trait]
impl Light for RgbCctLight {
    fn id(&self) -> &str {
        &self.id
    }

    fn current_state(&self) -> LightState {
        self.state.clone()
    }

    fn meta(&self) -> &DeviceMetadata {
        &self.meta
    }

    async fn apply_initial_state(&mut self) -> Result<(), Box<dyn Error>> {
        match self.state {
            LightState::Off(_) => {
                self.set_red(0f32)?;
                self.set_green(0f32)?;
                self.set_blue(0f32)?;
                self.set_cold(0f32)?;
                self.set_warm(0f32)?;
            }
            LightState::ColorTemp(state) => {
                self.apply_cct_state(state)?;
            }
            LightState::Rgbww(state) => {
                self.apply_rgbww_state(state)?;
            }
            LightState::Rgbw(state) => {
                self.apply_rgbw_state(state)?;
            }
            LightState::Rgb(state) => {
                self.apply_rgb_state(state)?;
            }
            _ => {
                return Err(Box::new(RgbCctLightError::InvalidCurrentState));
            }
        }
        Ok(())
    }

    async fn apply_new_state(&mut self, json: String) -> Result<(), Box<dyn Error>> {
        let cmd = serde_json::from_str::<HassLightRgbCmd>(&json)?;
        log::debug!("Applying new state for RGB-CCT light: {:?}", cmd);

        let on_off_state: bool = cmd.state.into();

        if !on_off_state {
            if let LightState::Off(_) = self.state {
                return Ok(());
            }

            self.set_red(0f32)?;
            self.set_green(0f32)?;
            self.set_blue(0f32)?;
            self.set_cold(0f32)?;
            self.set_warm(0f32)?;
            self.state = LightState::Off(Some(Box::from(self.state.clone())))
        } else if cmd.brightness.is_some() {
            let brightness = cmd.brightness.unwrap();
            let mut state = self.current_state();

            if cmd.color_temp.is_some() {
                state = LightState::ColorTemp(CctState {
                    brightness,
                    color_temp: cmd.color_temp.unwrap(),
                })
            } else if cmd.color.is_some() {
                let color = cmd.color.unwrap();
                if color.c.is_some() {
                    state = LightState::Rgbww(RgbwwState {
                        brightness,
                        red: color.r,
                        green: color.g,
                        blue: color.b,
                        cold: color.c.unwrap(),
                        warm: color.w.unwrap(),
                    });
                } else if color.w.is_some() {
                    state = LightState::Rgbw(RgbwState {
                        brightness,
                        red: color.r,
                        green: color.g,
                        blue: color.b,
                        white: color.w.unwrap(),
                    });
                } else {
                    state = LightState::Rgb(RgbState {
                        brightness,
                        red: color.r,
                        green: color.g,
                        blue: color.b,
                    });
                }
            } else if let LightState::Off(last_state) = state {
                if let Some(last_state) = last_state {
                    state = *last_state;
                } else {
                    state = LightState::ColorTemp(CctState {
                        brightness,
                        color_temp: 51,
                    });
                }
            }

            match state {
                LightState::ColorTemp(state) => {
                    let mut new_state = state;
                    new_state.brightness = brightness;
                    self.apply_cct_state(new_state)?;
                }
                LightState::Rgbww(state) => {
                    let mut new_state = state;
                    new_state.brightness = brightness;
                    self.apply_rgbww_state(new_state)?;
                }
                LightState::Rgbw(state) => {
                    let mut new_state = state;
                    new_state.brightness = brightness;
                    self.apply_rgbw_state(new_state)?;
                }
                LightState::Rgb(state) => {
                    let mut new_state = state;
                    new_state.brightness = brightness;
                    self.apply_rgb_state(new_state)?;
                }
                _ => {
                    return Err(Box::new(RgbCctLightError::InvalidCurrentState));
                }
            }
        } else if cmd.color.is_some() {
            let color = cmd.color.unwrap();
            let brightness = cmd
                .brightness
                .unwrap_or_else(|| get_brightness_from_state(self.current_state()));
            if color.c.is_some() {
                let new_state = RgbwwState {
                    brightness,
                    red: color.r,
                    green: color.g,
                    blue: color.b,
                    cold: color.c.unwrap(),
                    warm: color.w.unwrap(),
                };
                self.apply_rgbww_state(new_state)?;
            } else if color.w.is_some() {
                let new_state = RgbwState {
                    brightness,
                    red: color.r,
                    green: color.g,
                    blue: color.b,
                    white: color.w.unwrap(),
                };
                self.apply_rgbw_state(new_state)?;
            } else {
                let new_state = RgbState {
                    brightness,
                    red: color.r,
                    green: color.g,
                    blue: color.b,
                };
                self.apply_rgb_state(new_state)?;
            }
        } else if cmd.color_temp.is_some() {
            let brightness = cmd
                .brightness
                .unwrap_or_else(|| get_brightness_from_state(self.current_state()));
            let new_state = CctState {
                brightness,
                color_temp: cmd.color_temp.unwrap(),
            };
            self.apply_cct_state(new_state)?;
        } else if let LightState::Off(last_state) = self.current_state() {
            if let Some(last_state) = last_state {
                match *last_state {
                    LightState::ColorTemp(state) => {
                        self.apply_cct_state(state)?;
                    }
                    LightState::Rgbww(state) => {
                        self.apply_rgbww_state(state)?;
                    }
                    LightState::Rgbw(state) => {
                        self.apply_rgbw_state(state)?;
                    }
                    LightState::Rgb(state) => {
                        self.apply_rgb_state(state)?;
                    }
                    _ => {
                        return Err(Box::new(RgbCctLightError::InvalidLastState));
                    }
                }
            } else {
                let delta_mired = self.color_temp_config.min_mireds
                    + ((self.color_temp_config.max_mireds - self.color_temp_config.min_mireds) / 2);
                let state = CctState {
                    brightness: 255,
                    color_temp: delta_mired,
                };
                self.apply_cct_state(state)?;
            }
        }

        Ok(())
    }

    fn create_light_config(&self) -> HassLightConfig {
        HassLightConfig {
            brightness: true,
            color_mode: true,
            supported_color_modes: Some(vec![
                ColorMode::Rgbww,
                ColorMode::Rgbw,
                ColorMode::Rgb,
                ColorMode::ColorTemp,
            ]), // TODO: Support other modes
            min_mireds: Some(self.color_temp_config.min_mireds),
            max_mireds: Some(self.color_temp_config.max_mireds),
        }
    }
}

unsafe impl Sync for RgbCctLight {}

unsafe impl Send for RgbCctLight {}

#[derive(Error, Debug)]
pub enum RgbCctLightError {
    #[error("invalid last state before off")]
    InvalidLastState,
    #[error("invalid current state")]
    InvalidCurrentState,
    #[error("invalid mireds delviered in command")]
    InvalidMiredCommand,
}
