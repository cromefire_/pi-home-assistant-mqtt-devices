use super::{get_channel_brightness, ColorMode, Light, LightState, PwmChannel, RgbwState};
use crate::config::DeviceMetadata;
use crate::hass::lights::{HassLightConfig, HassLightRgbCmd};
use async_trait::async_trait;
use std::error::Error;
use std::sync::{Arc, Mutex};
use thiserror::Error;

pub struct RgbwLight {
    id: String,
    red: Arc<Mutex<PwmChannel>>,
    green: Arc<Mutex<PwmChannel>>,
    blue: Arc<Mutex<PwmChannel>>,
    white: Arc<Mutex<PwmChannel>>,
    state: LightState,
    meta: DeviceMetadata,
}

impl RgbwLight {
    pub fn new(
        id: String,
        red: PwmChannel,
        green: PwmChannel,
        blue: PwmChannel,
        white: PwmChannel,
        state: LightState,
        meta: DeviceMetadata,
    ) -> RgbwLight {
        RgbwLight {
            id,
            red: Arc::new(Mutex::new(red)),
            green: Arc::new(Mutex::new(green)),
            blue: Arc::new(Mutex::new(blue)),
            white: Arc::new(Mutex::new(white)),
            state,
            meta,
        }
    }

    fn set_red(&self, value: f32) -> Result<(), Box<dyn Error>> {
        let red = self.red.lock().unwrap();
        red.set_value(value)
    }

    fn set_green(&self, value: f32) -> Result<(), Box<dyn Error>> {
        let green = self.green.lock().unwrap();
        green.set_value(value)
    }

    fn set_blue(&self, value: f32) -> Result<(), Box<dyn Error>> {
        let blue = self.blue.lock().unwrap();
        blue.set_value(value)
    }

    fn set_white(&self, value: f32) -> Result<(), Box<dyn Error>> {
        let white = self.white.lock().unwrap();
        white.set_value(value)
    }

    fn apply_rgbw_state(&mut self, state: RgbwState) -> Result<(), Box<dyn Error>> {
        let red_value = get_channel_brightness(state.red, state.brightness);
        let green_value = get_channel_brightness(state.green, state.brightness);
        let blue_value = get_channel_brightness(state.blue, state.brightness);
        let white_value = get_channel_brightness(state.white, state.brightness);

        self.set_red(red_value)?;
        self.set_green(green_value)?;
        self.set_blue(blue_value)?;
        self.set_white(white_value)?;
        self.state = LightState::Rgbw(state);

        Ok(())
    }
}

#[async_trait]
impl Light for RgbwLight {
    fn id(&self) -> &str {
        &self.id
    }

    fn current_state(&self) -> LightState {
        self.state.clone()
    }

    fn meta(&self) -> &DeviceMetadata {
        &self.meta
    }

    async fn apply_initial_state(&mut self) -> Result<(), Box<dyn Error>> {
        match self.state {
            LightState::Off(_) => {
                self.set_red(0f32)?;
                self.set_green(0f32)?;
                self.set_blue(0f32)?;
                self.set_white(0f32)?;
            }
            LightState::Rgbw(state) => {
                self.apply_rgbw_state(state)?;
            }
            _ => {
                return Err(Box::new(RgbwLightError::InvalidCurrentState));
            }
        }
        Ok(())
    }

    async fn apply_new_state(&mut self, json: String) -> Result<(), Box<dyn Error>> {
        let cmd = serde_json::from_str::<HassLightRgbCmd>(&json).unwrap();
        log::debug!("Applying new state for RGBW light: {:?}", cmd);

        let on_off_state: bool = cmd.state.into();

        if !on_off_state {
            if let LightState::Off(_) = self.state {
                return Ok(());
            }

            self.set_red(0f32)?;
            self.set_green(0f32)?;
            self.set_blue(0f32)?;
            self.set_white(0f32)?;
            self.state = LightState::Off(Some(Box::from(self.state.clone())))
        } else if cmd.brightness.is_some() || cmd.color.is_some() {
            if let LightState::Rgbw(rgbw_state) = &self.state {
                let new_state = RgbwState {
                    brightness: cmd.brightness.unwrap_or(rgbw_state.brightness),
                    red: cmd.color.map(|c| c.r).unwrap_or(rgbw_state.red),
                    green: cmd.color.map(|c| c.g).unwrap_or(rgbw_state.green),
                    blue: cmd.color.map(|c| c.b).unwrap_or(rgbw_state.blue),
                    white: cmd.color.and_then(|c| c.w).unwrap_or(rgbw_state.white),
                };
                self.apply_rgbw_state(new_state)?;
            } else if let LightState::Off(last_state) = &self.state {
                if let Some(last_state) = last_state {
                    let last_state: &LightState = last_state;
                    if let LightState::Rgbw(rgbw_state) = last_state {
                        let new_state = RgbwState {
                            brightness: cmd.brightness.unwrap_or(rgbw_state.brightness),
                            red: cmd.color.map(|c| c.r).unwrap_or(rgbw_state.red),
                            green: cmd.color.map(|c| c.g).unwrap_or(rgbw_state.green),
                            blue: cmd.color.map(|c| c.b).unwrap_or(rgbw_state.blue),
                            white: cmd.color.and_then(|c| c.w).unwrap_or(rgbw_state.white),
                        };
                        self.apply_rgbw_state(new_state)?;
                    } else {
                        return Err(Box::from(RgbwLightError::InvalidLastState));
                    }
                } else {
                    let new_state = RgbwState {
                        brightness: cmd.brightness.unwrap_or(100),
                        red: cmd.color.map(|c| c.r).unwrap_or(255),
                        green: cmd.color.map(|c| c.g).unwrap_or(255),
                        blue: cmd.color.map(|c| c.b).unwrap_or(255),
                        white: cmd.color.and_then(|c| c.w).unwrap_or(255),
                    };
                    self.apply_rgbw_state(new_state)?;
                }
            } else {
                return Err(Box::from(RgbwLightError::InvalidCurrentState));
            }
        } else if let LightState::Off(last_state) = &self.state {
            if let Some(last_state) = last_state {
                let last_state: LightState = *last_state.clone();
                if let LightState::Rgbw(rgbw_state) = &last_state {
                    self.apply_rgbw_state(*rgbw_state)?;
                } else {
                    return Err(Box::from(RgbwLightError::InvalidLastState));
                }
            } else {
                let state = RgbwState {
                    brightness: 100,
                    red: 255,
                    green: 255,
                    blue: 255,
                    white: 255,
                };
                self.apply_rgbw_state(state)?;
            }
        }

        Ok(())
    }

    fn create_light_config(&self) -> HassLightConfig {
        HassLightConfig {
            brightness: true,
            color_mode: true,
            supported_color_modes: Some(vec![ColorMode::Rgbw, ColorMode::Rgbw]),
            min_mireds: None,
            max_mireds: None,
        }
    }
}

unsafe impl Sync for RgbwLight {}

unsafe impl Send for RgbwLight {}

#[derive(Error, Debug)]
pub enum RgbwLightError {
    #[error("invalid last state before off")]
    InvalidLastState,
    #[error("invalid current state")]
    InvalidCurrentState,
}
