use super::{get_channel_brightness, ColorMode, Light, LightState, PwmChannel, RgbState};
use crate::config::DeviceMetadata;
use crate::hass::lights::{HassLightConfig, HassLightRgbCmd};
use async_trait::async_trait;
use std::error::Error;
use std::sync::{Arc, Mutex};
use thiserror::Error;

pub struct RgbLight {
    id: String,
    red: Arc<Mutex<PwmChannel>>,
    green: Arc<Mutex<PwmChannel>>,
    blue: Arc<Mutex<PwmChannel>>,
    state: LightState,
    meta: DeviceMetadata,
}

impl RgbLight {
    pub fn new(
        id: String,
        red: PwmChannel,
        green: PwmChannel,
        blue: PwmChannel,
        state: LightState,
        meta: DeviceMetadata,
    ) -> RgbLight {
        RgbLight {
            id,
            red: Arc::new(Mutex::new(red)),
            green: Arc::new(Mutex::new(green)),
            blue: Arc::new(Mutex::new(blue)),
            state,
            meta,
        }
    }

    fn set_red(&self, value: f32) -> Result<(), Box<dyn Error>> {
        let red = self.red.lock().unwrap();
        red.set_value(value)
    }

    fn set_green(&self, value: f32) -> Result<(), Box<dyn Error>> {
        let green = self.green.lock().unwrap();
        green.set_value(value)
    }

    fn set_blue(&self, value: f32) -> Result<(), Box<dyn Error>> {
        let blue = self.blue.lock().unwrap();
        blue.set_value(value)
    }

    fn apply_rgb_state(&mut self, state: RgbState) -> Result<(), Box<dyn Error>> {
        let red_value = get_channel_brightness(state.red, state.brightness);
        let green_value = get_channel_brightness(state.green, state.brightness);
        let blue_value = get_channel_brightness(state.blue, state.brightness);

        self.set_red(red_value)?;
        self.set_green(green_value)?;
        self.set_blue(blue_value)?;
        self.state = LightState::Rgb(state);

        Ok(())
    }
}

#[async_trait]
impl Light for RgbLight {
    fn id(&self) -> &str {
        &self.id
    }

    fn current_state(&self) -> LightState {
        self.state.clone()
    }

    fn meta(&self) -> &DeviceMetadata {
        &self.meta
    }

    async fn apply_initial_state(&mut self) -> Result<(), Box<dyn Error>> {
        match self.state {
            LightState::Off(_) => {
                self.set_red(0f32)?;
                self.set_green(0f32)?;
                self.set_blue(0f32)?;
            }
            LightState::Rgb(state) => {
                self.apply_rgb_state(state)?;
            }
            _ => {
                return Err(Box::new(RgbLightError::InvalidCurrentState));
            }
        }
        Ok(())
    }

    async fn apply_new_state(&mut self, json: String) -> Result<(), Box<dyn Error>> {
        let cmd = serde_json::from_str::<HassLightRgbCmd>(&json).unwrap();
        log::debug!("Applying new state for RGB light: {:?}", cmd);

        let on_off_state: bool = cmd.state.into();

        if !on_off_state {
            if let LightState::Off(_) = self.state {
                return Ok(());
            }

            self.set_red(0f32)?;
            self.set_green(0f32)?;
            self.set_blue(0f32)?;
            self.state = LightState::Off(Some(Box::from(self.state.clone())))
        } else if cmd.brightness.is_some() || cmd.color.is_some() {
            if let LightState::Rgb(rgb_state) = &self.state {
                let new_state = RgbState {
                    brightness: cmd.brightness.unwrap_or(rgb_state.brightness),
                    red: cmd.color.map(|c| c.r).unwrap_or(rgb_state.red),
                    green: cmd.color.map(|c| c.g).unwrap_or(rgb_state.green),
                    blue: cmd.color.map(|c| c.b).unwrap_or(rgb_state.blue),
                };
                self.apply_rgb_state(new_state)?;
            } else if let LightState::Off(last_state) = &self.state {
                if let Some(last_state) = last_state {
                    let last_state: &LightState = last_state;
                    if let LightState::Rgb(rgb_state) = last_state {
                        let new_state = RgbState {
                            brightness: cmd.brightness.unwrap_or(rgb_state.brightness),
                            red: cmd.color.map(|c| c.r).unwrap_or(rgb_state.red),
                            green: cmd.color.map(|c| c.g).unwrap_or(rgb_state.green),
                            blue: cmd.color.map(|c| c.b).unwrap_or(rgb_state.blue),
                        };
                        self.apply_rgb_state(new_state)?;
                    } else {
                        return Err(Box::from(RgbLightError::InvalidLastState));
                    }
                } else {
                    let new_state = RgbState {
                        brightness: cmd.brightness.unwrap_or(100),
                        red: cmd.color.map(|c| c.r).unwrap_or(255),
                        green: cmd.color.map(|c| c.g).unwrap_or(255),
                        blue: cmd.color.map(|c| c.b).unwrap_or(255),
                    };
                    self.apply_rgb_state(new_state)?;
                }
            } else {
                return Err(Box::from(RgbLightError::InvalidCurrentState));
            }
        } else if let LightState::Off(last_state) = &self.state {
            if let Some(last_state) = last_state {
                let last_state: LightState = *last_state.clone();
                if let LightState::Rgb(rgb_state) = &last_state {
                    self.apply_rgb_state(*rgb_state)?;
                } else {
                    return Err(Box::from(RgbLightError::InvalidLastState));
                }
            } else {
                let state = RgbState {
                    brightness: 100,
                    red: 255,
                    green: 255,
                    blue: 255,
                };
                self.apply_rgb_state(state)?;
            }
        }

        Ok(())
    }

    fn create_light_config(&self) -> HassLightConfig {
        HassLightConfig {
            brightness: true,
            color_mode: true,
            supported_color_modes: Some(vec![ColorMode::Rgb]),
            min_mireds: None,
            max_mireds: None,
        }
    }
}

unsafe impl Sync for RgbLight {}

unsafe impl Send for RgbLight {}

#[derive(Error, Debug)]
pub enum RgbLightError {
    #[error("invalid last state before off")]
    InvalidLastState,
    #[error("invalid current state")]
    InvalidCurrentState,
}
