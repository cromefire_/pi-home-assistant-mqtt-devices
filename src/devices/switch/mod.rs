use crate::adc::Adc;
use crate::config::{DeviceMetadata, Switches};
use crate::devices::switch::analog::AnalogSwitch;
use crate::Config;
use async_std::sync::{Arc, Mutex};
use async_trait::async_trait;
use rumqttc::AsyncClient;
use std::collections::HashMap;
use std::error::Error;
use futures::executor::ThreadPool;

mod analog;

pub async fn load_switches(
    config: &Config,
    analog_inputs: &[Arc<Mutex<dyn Adc + Send>>],
) -> HashMap<String, Box<dyn Switch>> {
    let mut switch_devices: HashMap<String, Box<dyn Switch>> = HashMap::new();
    for (id, switch) in config.devices.switches.iter() {
        match switch {
            Switches::AnalogButton {
                analog,
                threshold,
                debouncing,
                meta,
            } => {
                let adc = analog_inputs
                    .get(analog.input as usize)
                    .unwrap_or_else(|| panic!("PWM module {} not found", analog.input));
                let channel = AnalogInputChannel::new(adc.clone(), analog.pin);
                let switch =
                    AnalogSwitch::new(id.clone(), channel, *threshold, *debouncing, meta.clone());
                switch_devices.insert(id.clone(), Box::new(switch));
            }
            // TODO
        }
    }
    switch_devices
}

#[async_trait]
pub trait Switch {
    fn id(&self) -> &str;
    fn current_state(&self) -> bool;
    fn meta(&self) -> &DeviceMetadata;
    async fn start_listener(&self, config: &Config, executor: std::sync::Arc<std::sync::Mutex<ThreadPool>>, client: Arc<Mutex<AsyncClient>>);
}

pub struct AnalogInputChannel {
    input: Arc<Mutex<dyn Adc + Send>>,
    pin: u8,
}

impl AnalogInputChannel {
    fn new(input: Arc<Mutex<dyn Adc + Send>>, pin: u8) -> AnalogInputChannel {
        AnalogInputChannel { input, pin }
    }

    async fn listen_analog(
        &self,
        callback: Box<dyn FnMut(u16) + Send>,
    ) -> Result<(), Box<dyn Error>> {
        let mut input = self.input.lock().await;
        input.listen_analog(self.pin, callback).await?;
        Ok(())
    }
}
