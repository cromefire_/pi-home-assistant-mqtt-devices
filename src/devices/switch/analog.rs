use std::sync::atomic::{AtomicU8, Ordering};
use crate::config::DeviceMetadata;
use crate::devices::switch::{AnalogInputChannel, Switch};
use crate::{Config, TopicHelper};
use async_std::sync::{Arc, Mutex};
use async_trait::async_trait;
use futures::executor::ThreadPool;
use futures::task::SpawnExt;
use log::error;
use rumqttc::{AsyncClient, QoS};

pub struct AnalogSwitch {
    id: String,
    channel: AnalogInputChannel,
    threshold: u16,
    debouncing: bool,
    meta: DeviceMetadata,
    state: bool,
}

impl AnalogSwitch {
    pub fn new(
        id: String,
        channel: AnalogInputChannel,
        threshold: u16,
        debouncing: bool,
        meta: DeviceMetadata,
    ) -> AnalogSwitch {
        AnalogSwitch {
            id,
            channel,
            threshold,
            debouncing,
            meta,
            state: false,
        }
    }
}

#[async_trait]
impl Switch for AnalogSwitch {
    fn id(&self) -> &str {
        &self.id
    }

    fn current_state(&self) -> bool {
        self.state
    }

    fn meta(&self) -> &DeviceMetadata {
        &self.meta
    }

    async fn start_listener(&self, config: &Config, executor: Arc<std::sync::Mutex<ThreadPool>>, client: Arc<Mutex<AsyncClient>>) {
        let threshold = self.threshold;
        let th = TopicHelper::new(config);
        let topic = th.tx("binary_sensor", &self.id);
        let last_value = Arc::new(Mutex::new(None));
        let fail_count = Arc::new(AtomicU8::new(0));
        self.channel
            .listen_analog(Box::new(move |val| {
                let fail_count = fail_count.clone();
                let topic = topic.clone();
                let last_value = last_value.clone();
                let client = client.clone();
                let future = async move {
                    let client = client.lock().await;
                    let mut last_value = last_value.lock().await;
                    let value = val < threshold;
                    if Some(value) == *last_value {
                        return;
                    } else {
                        *last_value = Some(value);
                    }
                    let payload = if value { "0" } else { "1" };
                    client
                        .publish(topic, QoS::AtLeastOnce, true, payload)
                        .await
                        .unwrap_or_else(|e| {
                            error!("Failed to send switch status: {:?}", e);
                            fail_count.fetch_add(1, Ordering::SeqCst);
                            if fail_count.load(Ordering::SeqCst) > 50 {
                                panic!("Failed to send switch status 50 times, giving up.");
                            }
                        });
                };
                let executor = executor.lock().unwrap();
                executor.spawn(future).unwrap();
            }))
            .await
            .unwrap();
    }
}
