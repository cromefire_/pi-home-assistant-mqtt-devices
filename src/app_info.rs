use lazy_static::lazy_static;
use rppal::system::DeviceInfo;
use std::fs::File;
use std::io::Read;
use std::ops::Deref;

pub const VERSION: &str = match option_env!("VERSION") {
    None => "unknown",
    Some(s) => s,
};

lazy_static! {
    static ref DEVICE_INFO: Option<DeviceInfo> = DeviceInfo::new().ok();
}

pub fn hardware_model() -> String {
    if let Some(info) = DEVICE_INFO.deref() {
        return info.model().to_string();
    }

    if let Ok(mut file) = File::open("/proc/device-tree/model") {
        let mut str = String::new();
        if file.read_to_string(&mut str).is_ok() {
            return str;
        }
    }

    "unknown".to_string()
}
